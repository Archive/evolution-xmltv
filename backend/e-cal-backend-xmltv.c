/* Evolution calendar - XMLTV backend
 *
 * Copyright (C) 2005 Novell, Inc (www.novell.com)
 *
 * Authors: Rodrigo Moya <rodrigo@novell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <libedata-cal/e-cal-backend-cache.h>
#include "e-cal-backend-xmltv.h"

struct _ECalBackendXMLTVPrivate {
	ECalBackendCache *cache;
};

static GObjectClass *parent_class = NULL;

static ECalBackendSyncStatus
e_cal_backend_xmltv_is_read_only (ECalBackendSync *backend, EDataCal *cal, gboolean *read_only)
{
	*read_only = TRUE;

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_cal_address (ECalBackendSync *backend, EDataCal *cal, char **address)
{
	/* XMLTV has no particular email addresses associated with it */
	*address = NULL;

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_alarm_email_address (ECalBackendSync *backend, EDataCal *cal, char **address)
{
	/* XMLTV has no particular email addresses associated with it */
	*address = NULL;

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_ldap_attribute (ECalBackendSync *backend, EDataCal *cal, char **attribute)
{
	*attribute = NULL;

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_static_capabilities (ECalBackendSync *backend, EDataCal *cal, char **capabilities)
{
	*capabilities = g_strdup (CAL_STATIC_CAPABILITY_NO_ALARM_REPEAT "," \
				  CAL_STATIC_CAPABILITY_NO_THISANDFUTURE  "," \
				  CAL_STATIC_CAPABILITY_NO_THISANDPRIOR);

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_open (ECalBackendSync *backend, EDataCal *cal, gboolean only_if_exists,
			  const char *username, const char *password)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_remove (ECalBackendSync *backend, EDataCal *cal)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_discard_alarm (ECalBackendSync *backend, EDataCal *cal, const char *uid, const char *auid)
{
	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_receive_objects (ECalBackendSync *backend, EDataCal *cal, const char *calobj)
{
	return GNOME_Evolution_Calendar_PermissionDenied;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_default_object (ECalBackendSync *backend, EDataCal *cal, char **object)
{
	return GNOME_Evolution_Calendar_UnsupportedMethod;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_object (ECalBackendSync *backend, EDataCal *cal, const char *uid, const char *rid, char **object)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_object_list (ECalBackendSync *backend, EDataCal *cal, const char *sexp_string, GList **objects)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_timezone (ECalBackendSync *backend, EDataCal *cal, const char *tzid, char **object)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_add_timezone (ECalBackendSync *backend, EDataCal *cal, const char *tzobj)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_set_default_timezone (ECalBackendSync *backend, EDataCal *cal, const char *tzid)
{
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_free_busy (ECalBackendSync *backend, EDataCal *cal, GList *users,
				     time_t start, time_t end, GList **freebusy)
{
	/* XMLTV doesn't count as busy time */
	icalcomponent *vfb = icalcomponent_new_vfreebusy ();
	icaltimezone *utc_zone = icaltimezone_get_utc_timezone ();
	char *calobj;

	icalcomponent_set_dtstart (vfb, icaltime_from_timet_with_zone (start, FALSE, utc_zone));
	icalcomponent_set_dtend (vfb, icaltime_from_timet_with_zone (end, FALSE, utc_zone));

	calobj = icalcomponent_as_ical_string (vfb);
	*freebusy = g_list_append (NULL, g_strdup (calobj));
	icalcomponent_free (vfb);

	return GNOME_Evolution_Calendar_Success;
}

static ECalBackendSyncStatus
e_cal_backend_xmltv_get_changes (ECalBackendSync *backend, EDataCal *cal, const char *change_id,
				   GList **adds, GList **modifies, GList **deletes)
{
	return GNOME_Evolution_Calendar_Success;
}

static gboolean
e_cal_backend_xmltv_is_loaded (ECalBackend *backend)
{
}

static void
e_cal_backend_xmltv_start_query (ECalBackend *backend, EDataCalView *query)
{
}

static CalMode
e_cal_backend_xmltv_get_mode (ECalBackend *backend)
{
}

static void
e_cal_backend_xmltv_set_mode (ECalBackend *backend, CalMode mode)
{
}

static icaltimezone *
e_cal_backend_xmltv_internal_get_default_timezone (ECalBackend *backend)
{
}

static icaltimezone *
e_cal_backend_xmltv_internal_get_timezone (ECalBackend *backend, const char *tzid)
{
}

/* Finalize handler for the xmltv backend */
static void
e_cal_backend_xmltv_finalize (GObject *object)
{
	ECalBackendXMLTV *cbtv;
	ECalBackendXMLTVPrivate *priv;

	g_return_if_fail (E_IS_CAL_BACKEND_XMLTV (object));

	cbtv = (ECalBackendXMLTV *) object;
	priv = cbtv->priv;

	if (priv->cache) {
		g_object_unref (priv->cache);
		priv->cache = NULL;
	}

	g_free (priv);
	cbtv->priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

/* Object initialization function for the XMLTV backend */
static void
e_cal_backend_xmltv_init (ECalBackendXMLTV *cbtv, ECalBackendXMLTVClass *klass)
{
	ECalBackendXMLTVPrivate *priv;

	priv = g_new0 (ECalBackendXMLTVPrivate, 1);

	cbtv->priv = priv;

	e_cal_backend_sync_set_lock (E_CAL_BACKEND_SYNC (cbtv), TRUE);
}

/* Class initialization function for the xmltv backend */
static void
e_cal_backend_xmltv_class_init (ECalBackendXMLTVClass *class)
{
	GObjectClass *object_class;
	ECalBackendClass *backend_class;
	ECalBackendSyncClass *sync_class;

	object_class = (GObjectClass *) class;
	backend_class = (ECalBackendClass *) class;
	sync_class = (ECalBackendSyncClass *) class;

	parent_class = (ECalBackendSyncClass *) g_type_class_peek_parent (class);

	object_class->finalize = e_cal_backend_xmltv_finalize;

	sync_class->is_read_only_sync = e_cal_backend_xmltv_is_read_only;
	sync_class->get_cal_address_sync = e_cal_backend_xmltv_get_cal_address;
	sync_class->get_alarm_email_address_sync = e_cal_backend_xmltv_get_alarm_email_address;
	sync_class->get_ldap_attribute_sync = e_cal_backend_xmltv_get_ldap_attribute;
	sync_class->get_static_capabilities_sync = e_cal_backend_xmltv_get_static_capabilities;
	sync_class->open_sync = e_cal_backend_xmltv_open;
	sync_class->remove_sync = e_cal_backend_xmltv_remove;
	sync_class->discard_alarm_sync = e_cal_backend_xmltv_discard_alarm;
	sync_class->receive_objects_sync = e_cal_backend_xmltv_receive_objects;
	sync_class->get_default_object_sync = e_cal_backend_xmltv_get_default_object;
	sync_class->get_object_sync = e_cal_backend_xmltv_get_object;
	sync_class->get_object_list_sync = e_cal_backend_xmltv_get_object_list;
	sync_class->get_timezone_sync = e_cal_backend_xmltv_get_timezone;
	sync_class->add_timezone_sync = e_cal_backend_xmltv_add_timezone;
	sync_class->set_default_timezone_sync = e_cal_backend_xmltv_set_default_timezone;
	sync_class->get_freebusy_sync = e_cal_backend_xmltv_get_free_busy;
	sync_class->get_changes_sync = e_cal_backend_xmltv_get_changes;
	backend_class->is_loaded = e_cal_backend_xmltv_is_loaded;
	backend_class->start_query = e_cal_backend_xmltv_start_query;
	backend_class->get_mode = e_cal_backend_xmltv_get_mode;
	backend_class->set_mode = e_cal_backend_xmltv_set_mode;

	backend_class->internal_get_default_timezone = e_cal_backend_xmltv_internal_get_default_timezone;
	backend_class->internal_get_timezone = e_cal_backend_xmltv_internal_get_timezone;
}

/**
 * e_cal_backend_xmltv_get_type:
 * @void: 
 *
 * Registers the #ECalBackendXMLTV class if necessary, and returns
 * the type ID associated to it.
 *
 * Return value: The type ID of the #ECalBackendXMLTV class.
 **/
GType
e_cal_backend_xmltv_get_type (void)
{
	static GType e_cal_backend_xmltv_type = 0;

	if (!e_cal_backend_xmltv_type) {
		static GTypeInfo info = {
			sizeof (ECalBackendXMLTVClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) e_cal_backend_xmltv_class_init,
			NULL, NULL,
			sizeof (ECalBackendXMLTV),
			0,
			(GInstanceInitFunc) e_cal_backend_xmltv_init
		};
		e_cal_backend_xmltv_type = g_type_register_static (E_TYPE_CAL_BACKEND_SYNC, "ECalBackendXMLTV", &info, 0);
	}

	return e_cal_backend_xmltv_type;
}
